//
//  ViewController.swift
//  G57L5
//
//  Created by Andrii on 10/11/17.
//  Copyright © 2017 Andrii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        /*
         * Задача 1.
         */
        HomeWork.task1(str: "Андрей")
        
        /*
         * Задача 2.
         */
        HomeWork.task2(str : "Николаевич")
        
        /*
         * Задача 3.
         */
        HomeWork.task3(string: "AndriiBondarchuk")
        
        /*
         * Задача 4.
         */
        HomeWork.task4(str: "Ось")
        
        /*
         * Задача 5.
         */
        HomeWork.task5(number: "1234567890")
        
        /*
         * Задача 6.
         */
        HomeWork.task6(password: "325@[5*6reWDr")
        
        /*
         * Задача 7.
         */
        var arrNumbers = [Int]()

        for _ in 0..<20 {
            arrNumbers.append(Int(arc4random()%10))
        }

        print(arrNumbers)

        print(HomeWork.arrSortAndRemoveDuplicates(arrNumbers: arrNumbers))
        
        /*
         * Задача 8.
         */
        print(HomeWork.convertStrToTranslite(str: "морДа"))
        
        /*
         * Задача 9.
         */
        print(HomeWork.arrSortByString(dataSource: ["lada", "sedan", "baklazhan"], searchString: "da"))
        
        /*
         * Задача 9 с sort using NSPredicate.
         */
        print(HomeWork.arrSortByStringWithNSPredicate(dataSource: ["lada", "sedan", "baklazhan"], searchString: "da"))
    }

}
