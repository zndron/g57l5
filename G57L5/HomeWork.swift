//
//  HomeWork.swift
//  G57L5
//
//  Created by Andrii on 10/11/17.
//  Copyright © 2017 Andrii. All rights reserved.
//

import UIKit

/*
 * Работа с String/Array
 */
class HomeWork: NSObject {
    
    /*
     * Задача 1.
     *
     * Создать строку с своим именем, вывести количество символов содержащихся в ней
     */
    static func task1(str: String) {
        
        print("'\(str)' содержит \(str.count) символов")
        
    }
    
    /*
     * Задача 2.
     *
     * Создать строку с своим отчеством, проверить его на окончание “ич/на”
     */
    static func task2(str: String) {
        
        if str.hasSuffix("на") {
            print("'\(str)' содержит окончание 'на'")
        }
        else if str.hasSuffix("ич") {
            print("'\(str)' содержит окончание 'ич'")
        }
        else {
            print("'\(str)' не содержит окончание 'ич/на'")
        }
        
    }
    
    /*
     * Задача 3.
     *
     * Создать строку где слитно написано Ваши ИмяФамилия “IvanVasilevich"
     * разбить на две отдельных строки из предыдущей создать третью где они
     * обе будут разделены пробелом
     *
     * Пример:
     * str1 = “Ivan”
     * str2 = “Vasilevich”
     * str3 = “Ivan Vasilevich"
     */
    static func task3(string: String) {
        
        var i = 0
        var word = ""
        var words = ""
        
        for char in string {
            
            let letter = String(char)
            
            if letter.lowercased() != letter && string.characters.first != char {
                print(word)
                words += words.isEmpty ? word : " " + word
                word = ""
                word.append(letter)
            }
            else {
                word.append(letter)
            }
            
            i += 1
            
            if i == string.count {
                print(word)
                words += words.isEmpty ? word : " " + word
            }
            
        }
        
        print(words)
        
    }
    
    /*
     * Задача 4.
     *
     * Вывести строку зеркально Ось -> ьсО не используя reverse (посимвольно)
     */
    static func task4(str: String) {
        
        var newStr = ""
        
        for i in 1...str.count {
            
            let ofsetBy = str.count - i
            let index = str.index(str.startIndex, offsetBy: ofsetBy)
            
            newStr.append(str[index])
        }
        
        print("зеркально \(str) -> \(newStr)")

    }
    
    /*
     * Задача 5.
     *
     * Добавить запятые в строку как их расставляет калькулятор
     *
     * Пример:
     * 1234567 -> 1,234,567
     * 12345 -> 12,345
     * (не использовать встроенный метод для применения формата)
     */
    static func task5(number: String) {
        
        var newNumber = ""
        
        for i in 1...number.characters.count {
            
            let ofsetBy = number.characters.count - i
            let index = number.index(number.startIndex, offsetBy: ofsetBy)
            
            if (i%3 == 0) && (i != number.characters.count) {
                newNumber = "," + String(number[index]) + newNumber
            }
            else {
                newNumber = String(number[index]) + newNumber
            }
        }
        
        print("формат \(number) -> \(newNumber)")
        
    }
    
    /*
     * Задача 6.
     *
     * Проверить пароль на надежность от 1 до 5
     * a) если пароль содержит числа +1
     * b) символы верхнего регистра +1
     * c) символы нижнего регистра +1
     * d) спец символы +1
     * e) если содержит все вышесказанное
     *
     * Пример:
     * 123456 - 1 a)
     * qwertyui - 1 c)
     * 12345qwerty - 2 a) c)
     * 32556reWDr - 3 a) b) c)
     */
    static func task6(password: String) {
        
        var points = 0
        
        var checkToDigit = false
        var checkToUppercase = false
        var checkToLowercase = false
        var checkToSpecialСharacters = false
        
        let specialCharacters = "@!?±#$%^&*()<>.,[]"
        
        for char in password {
        
            let letter = String(char)
            
            // проверка на числа
            if checkToDigit == false {
                
                for i in 0...9 {
                    
                    if String(i) == letter {
                        points += 1
                        checkToDigit = true
                        print("пароль содержит числа")
                    }
                    
                }
                
            }
            
            // проверка на символы верхнего регистра
            if letter.uppercased() == letter && checkToUppercase == false {
                points += 1
                checkToUppercase = true
                print("пароль содержит символы верхнего регистра")
            }
            
            // проверка на символы нижнего регистра
            if letter.lowercased() == letter && checkToLowercase == false {
                points += 1
                checkToLowercase = true
                print("пароль содержит символы нижнего регистра")
            }
            
            // проверка на спец символы
            if checkToSpecialСharacters == false {
                
                for specialChar in specialCharacters {
                    
                    let specialLetter = String(specialChar)
                    
                    if specialLetter == letter {
                        points += 1
                        checkToSpecialСharacters = true
                        print("пароль содержит спец символы")
                    }
                }
               
            }
            
        }
        
        if checkToDigit && checkToUppercase && checkToLowercase && checkToSpecialСharacters {
            points += 1
            print("пароль отвечает всем условиям")
        }
        
        // вывод надежности
        print("надежность пароля '\(password)' = \(points) (от 1 до 5)")
    }
    
    /*
     * Задача 7.
     *
     * Сортировка массива не встроенным методом по возрастанию + удалить дубликаты
     */
    static func arrSortAndRemoveDuplicates(arrNumbers: Array<Int>) -> Array<Int> {

        var arrSortedNumbers = [Int]()
        
        // удаление дубликатов
        for i in 0..<arrNumbers.count {
            if arrSortedNumbers.contains(arrNumbers[i]) == false {
                arrSortedNumbers.append(arrNumbers[i])
            }
        }
      
        // сортировка
        let size = arrSortedNumbers.count
        
        for i in 0..<size {
            
            let pass = (size - 1) - i
            
            for j in 0..<pass {
                
                if arrSortedNumbers[j] > arrSortedNumbers[j + 1] {
                    
                    let temp = arrSortedNumbers[j + 1]
                    
                    arrSortedNumbers[j + 1] = arrSortedNumbers[j]
                    arrSortedNumbers[j] = temp
                    
                }
                
            }
            
        }
        
        return arrSortedNumbers
        
    }
    
    /*
     * Задача 8.
     *
     * Написать метод который будет переводить строку в транслит
     *
     * Пример:
     * print(convertStrToTranslite(:”ЯЗЗЬ”)) -> “YAZZ”
     * print(convertStrToTranslite:”морДа”) -> “morDa”
     */
    static func convertStrToTranslite(str: String) -> String {
        
        var converter = [
            "а": "a",   "б": "b",   "в": "v",
            "г": "g",   "д": "d",   "е": "e",
            "ё": "e",   "ж": "zh",  "з": "z",
            "и": "i",   "й": "y",   "к": "k",
            "л": "l",   "м": "m",   "н": "n",
            "о": "o",   "п": "p",   "р": "r",
            "с": "s",   "т": "t",   "у": "u",
            "ф": "f",   "х": "h",   "ц": "c",
            "ч": "ch",  "ш": "sh",  "щ": "sch",
            "ь": "\'",  "ы": "y",   "ъ": "\'",
            "э": "e",   "ю": "yu",  "я": "ya",
            "А": "A",   "Б": "B",   "В": "V",
            "Г": "G",   "Д": "D",   "Е": "E",
            "Ё": "E",   "Ж": "ZH",  "З": "Z",
            "И": "I",   "Й": "Y",   "К": "K",
            "Л": "L",   "М": "M",   "Н": "N",
            "О": "O",   "П": "P",   "Р": "R",
            "С": "S",   "Т": "T",   "У": "U",
            "Ф": "F",   "Х": "H",   "Ц": "C",
            "Ч": "CH",  "Ш": "SH",  "Щ": "SCH",
            "Ь": "\'",  "Ы": "Y",   "Ъ": "\'",
            "Э": "E",   "Ю": "YU",  "Я": "YA"
        ]
    
        var translit = ""
        
        for char in str {
            
            if let letter = converter[String(char)] {
                translit.append(letter)
            }
            
        }
        
        return translit
        
    }
    
    /*
     * Задача 9.
     *
     * Сделать выборку из массива строк в которых содержится указанная строка
     *
     * Пример:
     * [“lada”, “sedan”, “baklazhan”] search “da” -> [“lada”, “sedan”] - sort() && sort using NSPredicate
     */
    static func arrSortByString(dataSource: Array<String>, searchString: String) -> Array<String> {
        
        var searchDataSource = [String]()
      
        for word in dataSource {
            if word.contains(searchString) {
                searchDataSource.append(word)
            }
        }
 
        return searchDataSource
        
    }
    
    /*
     * Задача 9 с sort using NSPredicate
     */
    static func arrSortByStringWithNSPredicate(dataSource: Array<String>, searchString: String) -> Any {

        let dataSource = dataSource as NSArray
        let namePredicate = NSPredicate(format: "SELF contains '\(searchString)'")
        let searchDataSource = dataSource.filtered(using: namePredicate)
        
        return searchDataSource
        
    }
    
}
